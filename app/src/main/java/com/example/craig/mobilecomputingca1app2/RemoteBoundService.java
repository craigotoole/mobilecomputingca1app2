package com.example.craig.mobilecomputingca1app2;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

/**
 * Created by craig on 28/02/2018.
 */

public class RemoteBoundService  extends Service {

    public static String stories = "";

    int dbnum = 0;
    String dbtags = "";

    final Messenger myMessenger = new Messenger(new GetStoryRequestHandler());

    static final int GET_STORIES = 0;

    public RemoteBoundService   () {

    }

    class GetStoryRequestHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case GET_STORIES:

                    Bundle bundle = new Bundle();
                    bundle.putString("key", stories);

                    Message reply = Message.obtain(null, GET_STORIES);
                    reply.obj = bundle;
                    try{
                        msg.replyTo.send(reply);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myMessenger.getBinder();
    }

    @Override
    public void onCreate() {

        Log.d("MobileApp", "Started");

        com.example.craig.mobilecomputingca1app2.SQLiteDatabase db = new com.example.craig.mobilecomputingca1app2.SQLiteDatabase(getApplicationContext());

        dbnum = db.getNumOfStories();
        dbtags = db.getTags();

        db.close();

    }

    @Override
    public void onDestroy(){

        Log.d("MobileApp", "Ended");


    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        Log.d("MobileApp", "Service started");

        final long period = 60000;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                FetchData events = new FetchData(getApplicationContext());
                try {

                    JSONArray result = events.execute().get();
                    Log.d("MobileApp", (result.length() - dbnum) + " new stories found.");
                    stories = result.toString();

                    int notificationNumber = 0;

                    String[] owntags = dbtags.split(", ");

                    if (result.length() - dbnum > 0){
                        for (int i = (result.length() - (result.length() - dbnum)); i < result.length(); i++) {

                            JSONObject holder = (JSONObject) result.get(i);

                            String[] tags = holder.getString("tags").split(", ");

                            for (int j = 0; j < tags.length; j++) {
                                for(int k = 0; k < owntags.length; k++){
                                    if (owntags[k].equals(tags[j])){
                                        notificationNumber++;
                                    }
                                }
                            }

                        }
                        dbnum = result.length();
                    }
                    if (notificationNumber > 0){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

                        NotificationCompat.Builder mBuilder =
                                new NotificationCompat.Builder(getApplicationContext())
                                        .setSmallIcon(R.drawable.ic_launcher_background)
                                        .setContentTitle(notificationNumber + " new stories with tags you follow have been posted!")
                                        .setContentText("Click here to view the stories!")
                                        .setContentIntent(pendingIntent);


                        NotificationManager mNotificationManager =

                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                        mNotificationManager.notify(001, mBuilder.build());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, 0, period);

    }

    public static String getStories() {
        return stories;
    }

}
