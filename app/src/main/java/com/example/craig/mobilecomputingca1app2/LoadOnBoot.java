package com.example.craig.mobilecomputingca1app2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by craig on 26/02/2018.
 */

public class LoadOnBoot extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        context.startService(new Intent(context, RemoteBoundService.class));

        Log.d("MobileApp", "Service loaded");

    }

}
